# Knowledge Base

## Git
**General info**
```
https://habr.com/ru/post/106912/
```
**Generate SSH-keys**
```
https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
```
**Configure multiple SSH-keys for different accounts**
```
https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab
```

## WebStorm
**Configure debugger**
```
https://stackoverflow.com/questions/19180702/how-can-i-run-nodemon-from-within-webstorm
```

## Configure WSL
```
https://itsfoss.com/install-bash-on-windows/
```
